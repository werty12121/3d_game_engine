class Vertex {
    constructor(x, y, z) {
        this.x = parseFloat(x);
        this.y = parseFloat(y);
        this.z = parseFloat(z);
    }
}
class Vertex2D {
    constructor(x, y) {
        this.x = parseFloat(x);
        this.y = parseFloat(y);
    }
}
class Wall {
    constructor(center, rotation, side, material) {
        this.type = "wall"
        this.rotation = rotation;
        var d = side / 2;
        this.d = d;
        this.side = side
        this.center = center;
        this.position = center;
        this.material = material;
        if (material instanceof CubeTextureMaterial) {
            this.materialtype = 'texture'
        } else if (material instanceof CubeCorrectTextureMaterial) {
            this.materialtype = 'correcttexture'
        } else {
            this.materialtype = 'color'
        }
        this.vertices = [
            new Vertex(center.x - d, center.y - d, center.z + d),
            new Vertex(center.x - d, center.y - d, center.z - d),
            new Vertex(center.x + d, center.y - d, center.z - d),
            new Vertex(center.x + d, center.y - d, center.z + d),
            new Vertex(center.x + d, center.y + d, center.z + d),
            new Vertex(center.x + d, center.y + d, center.z - d),
            new Vertex(center.x - d, center.y + d, center.z - d),
            new Vertex(center.x - d, center.y + d, center.z + d)
        ];

        this.setRotation(rotation)
        this.setRotationPlayerBased()
        this.faces = [
            [this.vertices[0], this.vertices[1], this.vertices[2], this.vertices[3]],
            [this.vertices[3], this.vertices[2], this.vertices[5], this.vertices[4]],
            [this.vertices[4], this.vertices[5], this.vertices[6], this.vertices[7]],
            [this.vertices[7], this.vertices[6], this.vertices[1], this.vertices[0]],
            //[this.vertices[7], this.vertices[0], this.vertices[3], this.vertices[4]],
            //[this.vertices[1], this.vertices[6], this.vertices[5], this.vertices[2]]
        ];
        if (material != undefined) {
            this.textures = [
                material.front, material.right, material.back, material.left, material.top, material.bottom
            ];
        }

    }
    setRotation(rotation) {

        for (var i = 0; i < this.vertices.length; i++) {
            this.vertices[i].x -= this.center.x
            this.vertices[i].y -= this.center.y
            this.vertices[i].z -= this.center.z
            var p_m = new Matrix([[this.vertices[i].x, this.vertices[i].y, this.vertices[i].z]])

            p_m.multiply(support.macierz_obrotu(this.rotation))

            var w = p_m.matrix
            this.vertices[i] = new Vertex(w[0][0], w[0][1], w[0][2])
            this.vertices[i].x += this.center.x
            this.vertices[i].y += this.center.y
            this.vertices[i].z += this.center.z

        }

    }
    setRotationPlayerBased() {


        for (var i = 0; i < this.vertices.length; i++) {
            var p_m = new Matrix([[this.vertices[i].x, this.vertices[i].y, this.vertices[i].z]])


            p_m.multiply(support.macierz_obrotu(playerrotation))

            var w = p_m.matrix

            this.vertices[i] = new Vertex(w[0][0], w[0][1], w[0][2])

        }
        var p_m = new Matrix([[this.center.x, this.center.y, this.center.z]])
        p_m.multiply(support.macierz_obrotu(playerrotation))
        var w = p_m.matrix

        this.center = new Vertex(w[0][0], w[0][1], w[0][2])

    }
}
class Plane {
    constructor(center, rotation, side, material) {
        this.type = "plane"
        this.rotation = rotation;
        this.center = center;
        var d = side / 2;
        this.side = side
        this.material = material
        if (material instanceof PlaneTextureMaterial) {
            this.materialtype = 'texture'
        } else if (material instanceof PlaneBasicMaterial) {
            this.materialtype = 'color'
        }
        this.vertices = [
            new Vertex(center.x - d, center.y - d, center.z - 0.1),
            new Vertex(center.x - d, center.y + d, center.z - 0.1),
            new Vertex(center.x + d, center.y + d, center.z - 0.1),
            new Vertex(center.x + d, center.y - d, center.z - 0.1)
        ];

        this.setRotation(rotation)
        this.setRotationPlayerBased()
        this.faces = [
            [this.vertices[0], this.vertices[1], this.vertices[2], this.vertices[3]]
        ];
        if (material != undefined) {
            this.textures = [
                material.front
            ];
        }
    }
    setRotation(rotation) {

        for (var i = 0; i < this.vertices.length; i++) {
            this.vertices[i].x -= this.center.x
            this.vertices[i].y -= this.center.y
            this.vertices[i].z -= this.center.z
            var p_m = new Matrix([[this.vertices[i].x, this.vertices[i].y, this.vertices[i].z]])

            p_m.multiply(support.macierz_obrotu(this.rotation))

            var w = p_m.matrix
            this.vertices[i] = new Vertex(w[0][0], w[0][1], w[0][2])
            this.vertices[i].x += this.center.x
            this.vertices[i].y += this.center.y
            this.vertices[i].z += this.center.z

        }

    }
    setRotationPlayerBased() {


        for (var i = 0; i < this.vertices.length; i++) {
            var p_m = new Matrix([[this.vertices[i].x, this.vertices[i].y, this.vertices[i].z]])


            p_m.multiply(support.macierz_obrotu(playerrotation))

            var w = p_m.matrix

            this.vertices[i] = new Vertex(w[0][0], w[0][1], w[0][2])

        }
        var p_m = new Matrix([[this.center.x, this.center.y, this.center.z]])
        p_m.multiply(support.macierz_obrotu(playerrotation))
        var w = p_m.matrix

        this.center = new Vertex(w[0][0], w[0][1], w[0][2])

    }
}
class Cube {
    constructor(center, rotation, side, material) {
        this.type = "cube"
        this.rotation = rotation;
        var d = side / 2;
        this.d = d;
        this.side = side
        this.center = center;
        this.position = center;
        this.material = material;
        if (material instanceof CubeTextureMaterial) {
            this.materialtype = 'texture'
        } else if (material instanceof CubeCorrectTextureMaterial) {
            this.materialtype = 'correcttexture'
        } else {
            this.materialtype = 'color'
        }
        this.vertices = [
            new Vertex(center.x - d, center.y - d, center.z + d),
            new Vertex(center.x - d, center.y - d, center.z - d),
            new Vertex(center.x + d, center.y - d, center.z - d),
            new Vertex(center.x + d, center.y - d, center.z + d),
            new Vertex(center.x + d, center.y + d, center.z + d),
            new Vertex(center.x + d, center.y + d, center.z - d),
            new Vertex(center.x - d, center.y + d, center.z - d),
            new Vertex(center.x - d, center.y + d, center.z + d)
        ];

        this.setRotation(rotation)
        this.setRotationPlayerBased()
        this.faces = [
            [this.vertices[0], this.vertices[1], this.vertices[2], this.vertices[3]],
            [this.vertices[3], this.vertices[2], this.vertices[5], this.vertices[4]],
            [this.vertices[4], this.vertices[5], this.vertices[6], this.vertices[7]],
            [this.vertices[7], this.vertices[6], this.vertices[1], this.vertices[0]],
            [this.vertices[7], this.vertices[0], this.vertices[3], this.vertices[4]],
            [this.vertices[1], this.vertices[6], this.vertices[5], this.vertices[2]]
        ];
        if (material != undefined) {
            this.textures = [
                material.front, material.right, material.back, material.left, material.top, material.bottom
            ];
        }

    }
    setRotation(rotation) {

        for (var i = 0; i < this.vertices.length; i++) {
            this.vertices[i].x -= this.center.x
            this.vertices[i].y -= this.center.y
            this.vertices[i].z -= this.center.z
            var p_m = new Matrix([[this.vertices[i].x, this.vertices[i].y, this.vertices[i].z]])

            p_m.multiply(support.macierz_obrotu(this.rotation))

            var w = p_m.matrix
            this.vertices[i] = new Vertex(w[0][0], w[0][1], w[0][2])
            this.vertices[i].x += this.center.x
            this.vertices[i].y += this.center.y
            this.vertices[i].z += this.center.z

        }

    }
    setRotationPlayerBased() {


        for (var i = 0; i < this.vertices.length; i++) {
            var p_m = new Matrix([[this.vertices[i].x, this.vertices[i].y, this.vertices[i].z]])


            p_m.multiply(support.macierz_obrotu(playerrotation))

            var w = p_m.matrix

            this.vertices[i] = new Vertex(w[0][0], w[0][1], w[0][2])

        }
        var p_m = new Matrix([[this.center.x, this.center.y, this.center.z]])
        p_m.multiply(support.macierz_obrotu(playerrotation))
        var w = p_m.matrix

        this.center = new Vertex(w[0][0], w[0][1], w[0][2])

    }
}


