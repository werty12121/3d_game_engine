class CubeMaterial {
    constructor(front, right, back, left, top, bottom) {
        this.front = front;
        this.right = right;
        this.back = back;
        this.left = left;
        this.top = top;
        this.bottom = bottom;
    }
}
class PlaneMaterial{
    constructor(front) {
        this.front = front;
    }
}
class CubeTextureMaterial extends CubeMaterial {
    constructor(front, right, back, left, top, bottom) {
        super(front, right, back, left, top, bottom)
    }
}
class CubeBasicMaterial extends CubeMaterial {
    constructor(front, right, back, left, top, bottom) {
        super(front, right, back, left, top, bottom)
    }
}
class CubeCorrectTextureMaterial extends CubeMaterial {
    constructor(front, right, back, left, top, bottom) {
        super(front, right, back, left, top, bottom)
    }
}
class PlaneTextureMaterial extends PlaneMaterial {
    constructor(front) {
        super(front)
    }
}
class PlaneBasicMaterial extends PlaneMaterial {
    constructor(front) {
        super(front)
    }
}